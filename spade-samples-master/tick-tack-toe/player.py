#Encoding: UTF-8
import spade
from random import choice


class Player(spade.Agent.Agent):

    class StartGameBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):
            self.msg = spade.ACLMessage.ACLMessage()
            self.msg.setPerformative('inform')
            self.msg.setOntology('start_game')
            self.msg.setContent('I am ready to start playing')
            self.msg.addReceiver(self.myAgent.tablero)

            self.myAgent.send(self.msg)
            self._exitcode = self.myAgent.TRANSLATION_TO_WAIT

    class WaitForGameBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):
            print self.myAgent.getName(),"I'm in Wait\n"
            self.msg = None
            self.msg = self._receive(True)

            if self.msg and self.msg.getOntology() == 'approve_start':
                print "approve_start"
                print "tablero from tablero.py",[int(i) for i in self.msg.content.split(",")],'\n'
                self.myAgent.boardNums=[int(i) for i in self.msg.content.split(",")]

                self.msg=None
                self._exitcode = self.myAgent.TRANSLATION_TO_GAME

            else:
                self._exitcode = self.myAgent.TRANSLATION_TO_DEFAULT

    class GameBehav(spade.Behaviour.OneShotBehaviour):

        def gen_and_send_decision(self):
            print "Make decision"
            decision = choice(self.myAgent.boardNums)
            print "decision: ", decision, '\n'
            self.msg = spade.ACLMessage.ACLMessage()
            self.msg.setOntology('choice')
            self.msg.setContent(decision)

            self.msg.addReceiver(self.myAgent.tablero)
            self.myAgent.send(self.msg)

        def _process(self):
            self.response = None
            self.response = self._receive(True)

            if self.response and self.response.getOntology() == 'game_end':
                self._exitcode = self.myAgent.TRANSLATION_TO_FINISH

            elif self.myAgent.jugat==0:
                self.myAgent.jugat=1
                print "[",self.myAgent.getName(),"]","I'm in Game"
                print "[",self.myAgent.getName(),"]",self.response.getOntology(), '\n'
                self.gen_and_send_decision()
                self._exitcode = self.myAgent.TRANSLATION_TO_WAIT

            else:
                print "ASIIII"
                self._exitcode = self.myAgent.TRANSLATION_TO_WAIT

    class EndGameBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):
            print "player exit..."
            self.myAgent._kill()

    def _setup(self):

        print "starting player..."

        self.tablero = spade.AID.aid(name='tablero@127.0.0.1', addresses=['xmpp://tablero@127.0.0.1'])
        #self.items = [0,1,2,3,4,5,6,7,8]

        self.STATE_START = 1
        self.STATE_WAIT = 2
        self.STATE_GAME = 3
        self.STATE_WINNER = 4

        self.TRANSLATION_TO_DEFAULT = 0
        self.TRANSLATION_TO_START = 10
        self.TRANSLATION_TO_WAIT = 20
        self.TRANSLATION_TO_GAME = 30
        self.TRANSLATION_TO_FINISH = 40
        self.jugat=0

        fsm = spade.Behaviour.FSMBehaviour()
        fsm.registerFirstState(self.StartGameBehav(), self.STATE_START)
        fsm.registerState(self.WaitForGameBehav(), self.STATE_WAIT)
        fsm.registerState(self.GameBehav(), self.STATE_GAME)
        fsm.registerLastState(self.EndGameBehav(), self.STATE_WINNER)

        fsm.registerTransition(self.STATE_START, self.STATE_START, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_START, self.STATE_WAIT, self.TRANSLATION_TO_WAIT)

        fsm.registerTransition(self.STATE_WAIT, self.STATE_WAIT, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_WAIT, self.STATE_GAME, self.TRANSLATION_TO_GAME)

        #fsm.registerTransition(self.STATE_GAME, self.STATE_GAME, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_GAME, self.STATE_WAIT, self.TRANSLATION_TO_WAIT)
        fsm.registerTransition(self.STATE_GAME, self.STATE_WINNER, self.TRANSLATION_TO_FINISH)

        self.setDefaultBehaviour(fsm)


if __name__ == '__main__':
    player_one = Player('player_one@127.0.0.1', 'secret')
    player_two = Player('player_two@127.0.0.1', 'secret')

    player_one.start()
    player_two.start()
