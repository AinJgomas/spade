#Encoding: UTF-8

import spade


class Tablero(spade.Agent.Agent):

    class WaitPlayersBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):

            self.msg = None
            self.msg = self._receive(True, 1)

            if self.msg and self.msg.getOntology() == 'start_game':
                self.myAgent.players_count += 1

                if self.myAgent.players_count >= 2:
                    self._exitcode = self.myAgent.TRANSLATION_TO_APPROVE
                    print "From Aprove to Game\n-------------------"
                else:
                    self._exitcode = self.myAgent.TRANSLATION_TO_DEFAULT
            else:
                self._exitcode = self.myAgent.TRANSLATION_TO_DEFAULT

    class GameApproveBehavPlayer2(spade.Behaviour.OneShotBehaviour):

        def _process(self):

            self.msg = None
            self.msg = spade.ACLMessage.ACLMessage()
            self.msg.setPerformative('inform')
            self.msg.setOntology('approve_start')
            print "Aprove player 2"
            print "ronda",self.myAgent.num_round

            self.msg.addReceiver(self.myAgent.player_two)
            print "string",str(self.myAgent.boardNums)[1:-1]

            self.msg.setContent(str(self.myAgent.boardNums)[1:-1])
            self.myAgent.send(self.msg)

            self._exitcode = self.myAgent.TRANSLATION_TO_GAME
            print "--------------"

    class GameApproveBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):

            self.msg = None
            self.msg = spade.ACLMessage.ACLMessage()
            self.msg.setPerformative('inform')
            self.msg.setOntology('approve_start')
            print "Aprove player 1"
            print "ronda",self.myAgent.num_round
            self.msg.addReceiver(self.myAgent.player_one)
            print "string",str(self.myAgent.boardNums)[1:-1]
            self.msg.setContent(str(self.myAgent.boardNums)[1:-1])
            self.myAgent.send(self.msg)

            self._exitcode = self.myAgent.TRANSLATION_TO_GAME
            print "--------------"

    class GameBehav(spade.Behaviour.OneShotBehaviour):

        SERVER = '127.0.0.1'

        def checkWin(self,player,board):
    	    print "checkwin", player
    	    print board
    	    return False

        def finish_game(self):
            self._exitcode = self.myAgent.TRANSLATION_TO_END

        def _process(self):

            self.msg = self._receive(True)
            if self.msg and self.msg.getOntology() == 'choice':
                ################# Received from player 1
                if self.msg.getSender().getName() == 'player_one@{0}'.format(self.SERVER):
                    self.myAgent.player_one_decision = self.msg.content
                    print "before quda aixi",self.myAgent.boardNums.remove(int(self.myAgent.player_one_decision))

                    #self.myAgent.boardNums[self.myAgent.player_one_decision)
                    print "quda aixi",self.myAgent.boardNums
                    self.myAgent.board[int(self.myAgent.player_one_decision)]='X'
                    print "board",self.myAgent.board
                    print self.myAgent.num_round
                    print
                    print 'player 1: {0}'.format(self.myAgent.board)
                    print 'Tablero:'
                    print   '-------------'
                    print ('| %s | %s | %s |'%(self.myAgent.board[0],self.myAgent.board[1],self.myAgent.board[2]))
                    print  '|-----------|'
                    print ('| %s | %s | %s |'%(self.myAgent.board[3],self.myAgent.board[4],self.myAgent.board[5]))
                    print  '|-----------|'
                    print ('| %s | %s | %s |'%(self.myAgent.board[6],self.myAgent.board[7],self.myAgent.board[8]))
                    print   '-------------'
                    print
                    #si guanya 1 self.finish_game()
                    print "print before check"
                    win = self.checkWin("player_one",self.myAgent.board)
                    print win
                    if bool(win):
                        self.finish_game()
                    else:
                        print "From Received 1 to Aprove player 2"
                        print "----------------------------------"
                        print
                        self.myAgent.num_round=2
                        self._exitcode = self.myAgent.TRANSLATION_TO_APPROVE2

                        ################# Received from player 2
                elif self.msg.getSender().getName() == 'player_two@{0}'.format(self.SERVER):
                    print "Recieved 2"
                    self.myAgent.player_two_decision = self.msg.content
                    print "Recieved 2_1"
                    print "bAbasns",self.myAgent.boardNums,"desicion",self.myAgent.player_two_decision
                    print "before quda aixi2",self.myAgent.boardNums.remove(int(self.myAgent.player_two_decision))
                    print "board",self.myAgent.board
                    self.myAgent.board[int(self.myAgent.player_two_decision)]='O'
                    self.myAgent.num_round=1
                    print
                    print 'player 2: {0}'.format(self.myAgent.board)
                    print 'Tablero:'
                    print   '-------------'
                    print ('| %s | %s | %s |'%(self.myAgent.board[0],self.myAgent.board[1],self.myAgent.board[2]))
                    print  '|-----------|'
                    print ('| %s | %s | %s |'%(self.myAgent.board[3],self.myAgent.board[4],self.myAgent.board[5]))
                    print  '|-----------|'
                    print ('| %s | %s | %s |'%(self.myAgent.board[6],self.myAgent.board[7],self.myAgent.board[8]))
                    print   '-------------'
                    print
                    #si guanya 2 self.finish_game()
                    if self.checkWin("player_two",self.myAgent.board):
                        self.finish_game()
                    else:
                        #Com no guanya ningú, continuem amb el jugador 1
                        self._exitcode = self.myAgent.TRANSLATION_TO_APPROVE
            else:
                self._exitcode = self.myAgent.TRANSLATION_TO_DEFAULT

    class EndBehav(spade.Behaviour.OneShotBehaviour):

        def _process(self):
            print "END!"
            self.msg = spade.ACLMessage.ACLMessage()
            self.msg.setOntology('game_end')
            self.msg.setContent('End of game!')

            self.msg.addReceiver(self.myAgent.player_one)
            self.msg.addReceiver(self.myAgent.player_two)
            self.myAgent.send(self.msg)
            self.myAgent._kill()

    def _setup(self):

        self.player_one = spade.AID.aid(name='player_one@127.0.0.1', addresses=['xmpp://player_one@127.0.0.1'])
        self.player_two = spade.AID.aid(name='player_two@127.0.0.1', addresses=['xmpp://player_two@127.0.0.1'])
        self.num_round = 1
        self.players_count = 0
        self.player_one_score = 0
        self.player_two_score = 0
        self.player_one_decision = None
        self.player_two_decision = None
        self.board=[]
        self.boardNums=[0,1,2,3,4,5,6,7,8]
        for i in range(0,9):
            self.board.append('C')

        self.STATE_WAIT = 1
        self.STATE_APPROVE = 2
        self.STATE_APPROVE2 = 3
        self.STATE_GAME = 4
        self.STATE_END = 5

        self.TRANSLATION_TO_DEFAULT = 0
        self.TRANSLATION_TO_APPROVE = 10
        self.TRANSLATION_TO_APPROVE2 = 11
        self.TRANSLATION_TO_GAME = 20
        self.TRANSLATION_TO_END = 30

        fsm = spade.Behaviour.FSMBehaviour()
        fsm.registerFirstState(self.WaitPlayersBehav(), self.STATE_WAIT)
        fsm.registerState(self.GameApproveBehav(), self.STATE_APPROVE)
        fsm.registerState(self.GameApproveBehavPlayer2(), self.STATE_APPROVE2)
        fsm.registerState(self.GameBehav(), self.STATE_GAME)
        fsm.registerLastState(self.EndBehav(), self.STATE_END)

        # Transitions from WAIT TO APROVE PLAYER 1
        fsm.registerTransition(self.STATE_WAIT, self.STATE_WAIT, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_WAIT, self.STATE_APPROVE, self.TRANSLATION_TO_APPROVE)

        # Transitions from APROVE PLAYER 1 TO GAME
        fsm.registerTransition(self.STATE_APPROVE, self.STATE_APPROVE, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_APPROVE, self.STATE_GAME, self.TRANSLATION_TO_GAME)

        # Transitions from APROVE PLAYER 2 TO GAME
        fsm.registerTransition(self.STATE_APPROVE2, self.STATE_APPROVE2, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_APPROVE2, self.STATE_GAME, self.TRANSLATION_TO_GAME)

        # Transitions from GAME 1 TO APROVE 2
        fsm.registerTransition(self.STATE_GAME, self.STATE_APPROVE2, self.TRANSLATION_TO_APPROVE2)
        fsm.registerTransition(self.STATE_GAME, self.STATE_APPROVE, self.TRANSLATION_TO_APPROVE)
        fsm.registerTransition(self.STATE_GAME, self.STATE_GAME, self.TRANSLATION_TO_DEFAULT)
        fsm.registerTransition(self.STATE_GAME, self.STATE_END, self.TRANSLATION_TO_END)

        self.setDefaultBehaviour(fsm)
if __name__ == '__main__':
    sender = Tablero('tablero@127.0.0.1', 'secret')
    #sender.setDebugToScreen()
    sender.start()
